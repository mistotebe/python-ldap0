**************************************************************
:py:mod:`ldap0.syncrepl` Implementation of a syncrepl consumer
**************************************************************

.. py:module:: ldap0.syncrepl
   :synopsis: Implementation of a syncrepl consumer

.. seealso::

   :rfc:`4533` - Lightweight Directory Access Protocol (v3): Content Synchronization Operation

This requires :py:mod:`pyasn1` and :py:mod:`pyasn1_modules` to be installed.


Classes
=======

This module defines the following classes:

.. autoclass:: ldap0.syncrepl.SyncreplConsumer
   :members:
